pkgname=jupiter-hw-support
_srctag=jupiter-20241205.1
_srcver=${_srctag#jupiter-}
pkgver=${_srcver//-/.}
pkgrel=3
pkgdesc="Jupiter HW support package"
arch=('any')
url="https://gitlab.com/evlaV/jupiter-hw-support"
license=('MIT')
depends=('python-evdev'
         'dmidecode' # for jupiter-biosupdate
         'python-crcmod' 'python-click' 'python-progressbar' 'python-hid'
         'jq' # for jupiter-controller-update, jupiter-biosupdate
         'alsa-utils' # for the sound workarounds
         'parted' 'e2fsprogs' # for sdcard formatting
         'udisks2>=2.9.4-1.1' # for mounting external drives with the 'as-user' option
         'ntfs-3g'
         'f3' #Used by format to check for bogus sdcards
        )

# Some pre-compiled binaries such as `rfp-cli` break when touched by `strip` :-\
options+=('!strip')
makedepends=('git' 'xorg-xcursorgen')
install=$pkgname.install
source=("git+https://gitlab.com/evlaV/jupiter-hw-support.git#tag=$_srctag"
        "steamos-automount.sh-btfs-support.patch"
        "biosupdate.patch"
        "fstrim.patch"
        "more-time.patch"
        "priv-write.patch"
        "udev-automount-rules.patch"
        "shebang.patch"
        "systemd-run.patch"
        "user.patch"
        "dock-updater.patch"
        "support-hw.patch"
        "pythonhid.patch")

sha256sums=('b52ce8a8d9c911d738bfc564f9faa9b3d877ce6fb6d6e99e66bc1e363b0143b0'
            'fe064240bf77dabfa4d161b10c33c82385a0ca22a49350e70ab9301b0e98dc7b'
            'a7105f9a18d808af94ea3803e1f77399653e78c7cf7ae1546dd3fe06b2b8c118'
            '032d71f114819280586ea76e84010897b69c505809fd87851d37db68f9b79094'
            '3b837e3521e143c52d234fb38075fdf328517d20634ea3fadfa99d6c5f4e095f'
            'd58747e9980bb4d5465d8a3649736383f5a79debe396ec8bc26015471a74cce0'
            'd6b5636eb09d2190b10eb665238c30960babba96424fc9768e36966495fa795e'
            'bf7ace7136722ecd96f72c3e4af60ffc6937a9e25818dfafe6f7e729bd9ca848'
            '82b5fbb167ae25f69230cde417c8b9086440b720f6259f5526feef1b41b57d6d'
            'c89ee8d097f047ec129d3c0a1121c625b718045722c547cb4002743820627494'
            '6dfef5d790f32e72dfa09ffaa136e0a82d36143148af021fa07d0a7778b9f2b5'
            '95595cd9f8150101983169f3ad1ad0e0194e4421ad3b40aa848f0c63eca0c3f2'
            '05f7a3d1fcff857aad4e5a8dae400f6acbf32c7e505db347f441dd33a6601df4')

prepare() {
  # Apply SteamOS Btrfs patch from https://gitlab.com/popsulfr/steamos-btrfs
  # And apply Bazzite patches from https://github.com/ublue-os/bazzite/tree/main/spec_files/jupiter-hw-support
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/steamos-automount.sh-btfs-support.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/systemd-run.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/user.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/biosupdate.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/fstrim.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/more-time.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/priv-write.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/udev-automount-rules.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/shebang.patch
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/support-hw.patch

  # Add jupiter-dock-updater-bin
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/dock-updater.patch

  # Add fix for controller FW updater (taken from CachyOS)
  patch -d "$srcdir/$pkgname" -p1 -i "${srcdir}"/pythonhid.patch
}

package() {

  cd "$srcdir/$pkgname/usr/share/steamos/"
  xcursorgen "$srcdir/$pkgname/usr/share/steamos/steamos-cursor-config" "$srcdir/$pkgname/usr/share/icons/steam/cursors/default"

  cd "$srcdir/$pkgname/usr/share/jupiter_bios_updater"

  # Remove gtk2 binary and respective build/start script - unused
  # Attempts to use gtk2 libraries which are not on the device.
  rm h2offt-g H2OFFTx64-G.sh

  # Driver module -- doesn't currently build, and not supported
  rm -rf driver
  rm -rf ${srcdir}/$pkgname/usr/lib/systemd/system/multi-user.target.wants

  cp -rv ${srcdir}/$pkgname/usr ${pkgdir}/usr
  cp -rv ${srcdir}/$pkgname/etc ${pkgdir}/etc
}
